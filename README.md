# Named entity recognition

This repository is for the university course BMEVITMMA06. The purpose of this project is to compare some basic neural architectures for named entity recognition. Additional experiments with pretrained transformer language models from [Huggingface/transformers](https://github.com/huggingface/transformers) is available in this [gist](https://gist.github.com/Mrpatekful/bad74821c9f2189c9b9362fd940d72a4).

## Usage

To train the model clone this repository and install dependecies. The project uses cython to assemble batches for faster input pipeline. Description about the model architectures is available in this [link]().

```console
pip install -r requirements.txt

python setup.py build_ext --inplace
```

The model can be trained with the following commands. Note that `<data_dir>` and `<model_dir>` are optional, as they are provided by default but you can also customize the location of model and data directories with those arguments.

```console
python -m src.scripts.train --model <MODEL>
```

To apply parameter sweep with grid search simply pass `--grid_search` argument to the above script. *( As a sidenote for customizability the hyperparameter list for the grid is fetched from the `src/configs` folder )*. Here is a simple script that applies parameter sweep on every model and dumps the results to `model/<data>/<model>/<date>-SEARCH` folder.

```bash
MODELS="rnn rnn_crf cnn cnn_crf trf trf_crf"
NAME=$(date +'%y.%m.%d-%H:%M:%S')-SEARCH

for m in $MODELS; do
    python -m src.scripts.train --model $m --name $NAME --grid_search
done
```

Details about the evaluation scripts and training is available in this [link]().

## Results

**coming soon**
