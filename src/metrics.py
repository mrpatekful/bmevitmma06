"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

# pylint: disable=no-member
# pylint: disable=not-callable

import os
import subprocess
import torch
import tempfile

from tqdm import tqdm

from skorch.callbacks import EpochScoring
from sklearn.metrics.scorer import (
    _check_multimetric_scoring,
    check_scoring)

from os.path import dirname, abspath, join


class MultiMetricEpochScoring(EpochScoring):
    """
    Custrom scoring class for returning
    mutiple metrics from a single score fn.
    """

    def _record_score(self, history, current_score_dict):
        """
        Record the current store and, if applicable, 
        if it's the best score yet.
        """
        for name, score in current_score_dict.items():
            score_name = self.name_ + '_' + name
            history.record(score_name, score)

            is_best = self._is_best_score(score)
            if is_best is None:
                return

            history.record(
                score_name + '_best', bool(is_best))
            if is_best:
                self.best_score_ = score

    def _scoring(self, net, X_test, y_test):
        """
        Resolve scoring and apply it to data. Use cached 
        prediction instead of running inference 
        again, if available.
        """
        scorer, _ = _check_multimetric_scoring(
            net, self.scoring_)
        return scorer['score'](net, X_test, y_test)


def compute_f1_score(
        preds, targets, num_classes, ignore_idx=-1):
    """
    Computes the multi-class f1 score of the 
    predictions.
    """
    targets = targets.view(-1)

    valid_indices = targets != ignore_idx
    targets = targets[valid_indices]
    preds = preds[valid_indices]

    labels = torch.arange(
        0, num_classes, dtype=torch.int64)
    labels = labels.unsqueeze(1)
    labels = labels.to(preds.device)

    preds = preds.unsqueeze(0).expand(
        num_classes, -1) == labels
    targets = targets.unsqueeze(0).expand(
        num_classes, -1) == labels

    recall = compute_recall(
        preds=preds, targets=targets)

    precision = compute_precision(
        preds=preds, targets=targets)

    f1_score = 2 * (precision * recall) / \
        (precision + recall)

    return f1_score


def compute_precision(preds, targets):
    """
    Computes the precision of the predictions.
    """
    true_positives = (preds & targets).sum(dim=-1)
    selected = preds.sum(dim=-1)

    precision = true_positives.float() / \
        selected.float()

    return precision


def compute_recall(preds, targets):
    """
    Computes the recall of the predictions.
    """
    true_positives = (preds & targets).sum(dim=-1)
    relevants = targets.sum(dim=-1)

    recall = true_positives.float() / \
        relevants.float()

    return recall


def compute_accuracy(preds, targets, ignore_idx=-1):
    """
    Computes the accuracy of the predictions.
    """
    targets = targets.view(-1)
    # computing accuracy without including
    # the values at the ignore indices
    not_ignore = targets.ne(ignore_idx)
    num_targets = not_ignore.long().sum()
    num_targets = num_targets.item()

    correct = (targets == preds) & not_ignore
    correct = correct.float().sum()

    accuracy = correct / num_targets

    return accuracy


@torch.no_grad()
def compute_conll_stats(
        model, dataset, idx_to_word, idx_to_tag):
    """
    Calls the conlleval script on the dataset.
    """
    with tempfile.NamedTemporaryFile('w') as fh:
        results = []

        for inputs, targets in tqdm(dataset):
            input_tensor = torch.as_tensor(
                inputs).unsqueeze(0)
            mask = torch.ones_like(input_tensor)

            preds = model.decode((input_tensor, mask))[0]

            results.append(''.join(
                '{} {} {}\n'.format(
                    idx_to_word[str(w)],
                    idx_to_tag[str(t)],
                    idx_to_tag[str(p)])
                for w, p, t in
                zip(inputs, preds, targets)))

        fh.write('\n'.join(results))
        fh.flush()

        command = '{} < {}'.format(
            join(abspath(dirname(__file__)),
                 'scripts', 'conlleval'),
            fh.name)

        result = subprocess.check_output(
            command, shell=True, stderr=subprocess.STDOUT)
        result = result.decode('utf-8')

    return result
