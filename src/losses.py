"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

import torch

from torch.nn.modules import NLLLoss

from torch.nn.functional import log_softmax, nll_loss


class SequenceNLLLoss(NLLLoss):
    """
    Negative log likelihood loss function for
    sequence of outputs.
    """

    def forward(self, inputs, targets):
        input_shape = -1, inputs.size(-1)
        input_view = inputs.reshape(input_shape)
        input_view = input_view.contiguous()

        log_probs = log_softmax(input_view, dim=-1)

        target_view = targets.view(-1)

        loss = nll_loss(
            log_probs, target_view, 
            ignore_index=self.ignore_index,
            reduction='sum')

        not_ignore = targets.ne(self.ignore_index)
        num_targets = not_ignore.long().sum().item()

        loss = loss / num_targets

        return loss


class SequenceLSLoss(NLLLoss):
    """
    Labels smoothed version of negative log
    likelihood loss for sequences.
    """
    pass


def create_criterion(
        pad_idx, vocab_size, device, smoothing=0.1):
    """
    Creates label smoothing loss with kl-divergence for the
    seq2seq model.
    """
    confidence = 1.0 - smoothing
    # initializes the target distribution vector with 
    # smoothing value divided by the number of other 
    # valid tokens
    smoothed = torch.full(
        (vocab_size, ), smoothing / (vocab_size - 2))
    smoothed = smoothed.to(device)
    smoothed[pad_idx] = 0

    def label_smoothing(outputs, targets):
        """
        Computes the kl-divergence between the preds and the 
        smoothed target distribution.
        """
        smoothed_targets = smoothed.repeat(targets.size(0), 1)
        smoothed_targets.scatter_(
            1, targets.unsqueeze(1), confidence)
        smoothed_targets.masked_fill_(
            (targets == pad_idx).unsqueeze(1), 0)

        return kl_div(outputs, smoothed_targets, 
                      reduction='sum')

    return label_smoothing
