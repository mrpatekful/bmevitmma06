"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

from torch.optim import SGD, AdamW


def setup_optim_args(parser):
    """
    Parses the optimizer related parameters.
    """
    parser.add_argument(
        '--optimizer',
        type=str,
        default='sgd',
        choices=list(OPTIMIZERS),
        help='Optimizer for the model.')

    args, _ = parser.parse_known_args()
    OPTIMIZERS[args.optimizer].setup_optim_args(parser)        


def create_optimizer(args):
    """
    Creates an optimizer and its params.
    """
    optim_cls = OPTIMIZERS[args.optimizer]
    optim_params = optim_cls.select_optim_params(
        vars(args))

    return optim_cls, optim_params


class OptimizerMixIn:
    """
    Mixin class ford discovering available
    optimizer classes.
    """

    @classmethod
    def select_optim_params(cls, params):
        return {
            arg: params[arg] for arg 
            in ['lr', 'weight_decay']
        }

    @classmethod
    def setup_optim_args(cls, parser):
        parser.add_argument(
            '--lr',
            type=float,
            default=1e-3,
            help='Learning rate for the model.')
        parser.add_argument(
            '--weight_decay',
            type=float,
            default=1e-6,
            help='Weight decay reguralization.')
        

class AdamW_(OptimizerMixIn, AdamW):
    """
    AdamW optimizer class that selects the
    relevant parameters for `torch.optim.AdamW`.
    """

    def __init__(self, params, lr=1e-3, 
                 betas=(0.9, 0.999), 
                 eps=1e-8, weight_decay=1e-2, 
                 amsgrad=False, **kwargs):
        AdamW.__init__(
                self, params, lr, betas, eps,
                weight_decay, amsgrad)


class SGD_(OptimizerMixIn, SGD):
    """
    AdamW optimizer class that selects the
    relevant parameters for `torch.optim.SGD`.
    """

    @classmethod
    def select_optim_params(cls, params):
        params_ = {
            arg: params[arg] for arg
            in ['nesterov', 'momentum']
        }

        params_.update(
            super().select_optim_params(params))
        
        return params_

    def __init__(self, params, lr=1e-3, momentum=0, 
                 dampening=0, weight_decay=0, 
                 nesterov=False, **kwargs):
        SGD.__init__(
                self, params, lr, momentum, dampening, 
                weight_decay, nesterov)

    @classmethod
    def setup_optim_args(cls, parser):
        super().setup_optim_args(parser)
        parser.add_argument(
            '--nesterov',
            action='store_true',
            help='Use nesterov momentum for training.')
        parser.add_argument(
            '--momentum',
            type=float,
            default=0,
            help='Momentum value for the SGD.')


OPTIMIZERS = {
    c.__name__[:-1].lower(): c for c in  
    OptimizerMixIn.__subclasses__()
}
