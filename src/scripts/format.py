"""
Converts the csv course format to conll format.

@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

import argparse


def generate_sentences(lines):
    """
    Generates sentences from a file.
    """
    sentence = []
    for line in lines:
        split = line.strip().split(';')

        if split[0] != '' and len(sentence) > 0:
            yield sentence

            sentence = []

        sentence.append(split[1:])

    if len(sentence) > 0:
        yield sentence


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--header',
        action='store_true',
        help='The file has a header.')
    parser.add_argument(
        '--newline',
        type=str,
        default='\n',
        help='Line separator.')
    parser.add_argument(
        '-f', '--file',
        type=str,
        help='Name of the file to convert.')

    args = parser.parse_args()

    with open(args.file, 'r') as ih, \
        open('out.txt', 'w') as oh:
        # removing the header
        if args.header:
            ih.readline()

        lines = ih.read().split(args.newline)

        oh.write('\n\n'.join('\n'.join(
            '\t'.join(l) for l in s)
            for s in generate_sentences(lines)))


if __name__ == '__main__':
    main()
