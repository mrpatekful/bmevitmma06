"""
Converts a conll formatted file to spacy's json NER format.

@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

import argparse
import json
import spacy


def generate_examples(file_handle):
    """
    Generates words and the NER word tags
    from a conll formatted file.
    """
    sentence, labels = [], []

    for line in file_handle:
        line = line.strip().split()
        if len(line) == 0:
            yield sentence, labels
            sentence, labels = [], []

        else:
            word, *_, label = line
            # currently using uncased model
            sentence.append(word.lower())
            labels.append(label)


def genereate_transformed(file_handle):
    """
    Generates transformed examples.
    """
    for sentence, labels in generate_examples(file_handle):
        sentence, entities = transform_examples(
            iter(zip(sentence, labels)))

        yield [sentence, {'entities': entities}]


def transform_examples(pairs):
    """
    Transforms an example to NER format.
    """
    # stores the words and entites for the sentence
    sentence, entities = '', []

    def _generate():
        # stores the currently processed word and entity
        words, prev_label = next(pairs)

        words += ' '
        prev_label = prev_label.split('-')[-1]

        for word, label in pairs:
            label = label.split('-')

            if label[0] != 'I':
                yield words, prev_label
                
                words = word + ' '
                prev_label = label[-1]
            
            else:
                words += word + ' '

    for words, entity in _generate():
        if entity != 'O':
            entity = [len(sentence), entity]
            sentence += words
            entity.insert(1, len(sentence))
            entities.append(entity)

        else:
            sentence += words
        
    return sentence[:-1], entities


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--header',
        action='store_true',
        help='The file has a header.')
    parser.add_argument(
        '--newline',
        type=str,
        default='\n',
        help='Line separator.')
    parser.add_argument(
        '-f', '--file',
        type=str,
        help='Name of the file to convert.')

    args = parser.parse_args()

    with open(args.file, 'r') as ih, \
        open('out.json', 'w') as oh:
        # removing the header
        if args.header:
            ih.readline()

        json.dump(list(genereate_transformed(ih)), oh)
    

if __name__ == '__main__':
    main()
