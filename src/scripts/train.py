"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

# pylint: disable=import-error
# pylint: disable=no-name-in-module
# pylint: disable=no-member
# pylint: disable=not-callable
# pylint: disable=used-before-assignment

import json
import torch
import argparse
import logging
import os
import re

import pandas as pd

from src.models import (
    create_model_cls,
    create_embed_cls,
    setup_model_args,
    score, multi_score,
    MODELS)

from src.data import (
    create_dataset,
    EntityDataset,
    setup_data_args)

from src.metrics import (
    compute_accuracy,
    compute_f1_score,
    compute_conll_stats,
    MultiMetricEpochScoring)

from src.utils import (
    create_params,
    parse_grid,
    set_random_seed)

from src.losses import SequenceNLLLoss

from src.optim import (
    setup_optim_args,
    create_optimizer)

from src.collate import padded_collate

from datetime import datetime
from functools import partial
from collections import Collection
from itertools import chain, product

from skorch.callbacks import (
    EpochScoring, ProgressBar,
    EarlyStopping, LRScheduler,
    Callback)

from torch.nn.functional import (
    nll_loss, log_softmax)

from torch.optim import AdamW, SGD
from torch.nn.modules import NLLLoss
from torch.utils.data import DataLoader

from sklearn.model_selection import (
    GridSearchCV)

from os.path import (
    exists, join, abspath, dirname)


def setup_train_args():
    """
    Sets up the training arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--param_grid',
        type=str,
        default=None,
        help='Path of the param_grid file.')
    parser.add_argument(
        '--max_epochs',
        type=int,
        default=100,
        help='Maximum number of epochs for training.')
    parser.add_argument(
        '--no_cuda',
        action='store_true',
        help='Device for training.')
    parser.add_argument(
        '--batch_size',
        type=int,
        default=128,
        help='Batch size during training.')
    parser.add_argument(
        '--patience',
        type=int,
        default=5,
        help='Number of patience epochs before termination.')
    # TODO unused currently
    parser.add_argument(
        '--grad_accum_steps',
        type=int,
        default=2,
        help='Number of steps for grad accum.')
    parser.add_argument(
        '--cross_folds',
        type=int,
        default=3,
        help='Cross validation folds for grid search.')
    parser.add_argument(
        '--seed',
        type=int,
        default=None,
        help='Random seed for the experiments.')

    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        '--warm_start',
        action='store_true',
        help='Start from a trained state.')
    group.add_argument(
        '--grid_search',
        action='store_true',
        help='Search the best hparams.')

    setup_model_args(parser)
    setup_data_args(parser)
    setup_optim_args(parser)

    return parser.parse_args()


def main():
    """
    Performs training, validation and testing.
    """
    args = setup_train_args()

    model_dir = join(
        args.model_dir, args.data, args.model,
        args.name)

    args.cuda = not args.no_cuda and \
        torch.cuda.is_available()

    if args.seed is not None:
        set_random_seed(args)

    device = torch.device(
        'cuda' if args.cuda else 'cpu')

    module_cls = create_model_cls(args)
    embed_cls = create_embed_cls(args)
    vocab_cls = embed_cls.get_vocab_cls()

    # creating dataset and storing dataset splits
    # as individual variables for convenience
    dataset, token_vocab, tag_vocab = \
        create_dataset(args, vocab_cls)

    vocab_size = len(token_vocab.token_to_idx)
    num_labels = len(tag_vocab.token_to_idx)

    # ignore idx is hardcoded value for now
    ignore_idx = -1

    model_file = join(model_dir, 'model.pt')
    optim_file = join(model_dir, 'optim.pt')
    hist_file = join(model_dir, 'history.json')

    # only use warm start if all files exist
    warm_start = exists(model_file) \
        and exists(optim_file) \
        and exists(hist_file) \
        and args.warm_start

    net_cls = module_cls.get_net_cls()

    optim_cls, optim_params = create_optimizer(args)

    # preparing metric fns for epoch logging
    accuracy_scoring = partial(
        score, metric_fn=compute_accuracy)

    # converting multi class f1 score metric
    # to binarized version
    compute_f1_score_fn = partial(
        compute_f1_score,
        num_classes=num_labels)

    f1_scoring = partial(
        multi_score,
        metric_fn=compute_f1_score_fn)

    # creating metric callback functions
    accuracy = EpochScoring(
        scoring=accuracy_scoring,
        lower_is_better=False,
        use_caching=False,
        name='valid_acc')

    f1_score = MultiMetricEpochScoring(
        scoring=f1_scoring,
        lower_is_better=False,
        use_caching=False,
        name='valid_f1')

    # learning rate schedule callback
    scheduler = LRScheduler()

    # early stopping
    early_stopping = EarlyStopping(
        patience=args.patience)

    # progress bar logging
    progress = ProgressBar()

    train_iterator_params = create_params(
        base='iterator_train',
        param_dict={
            'batch_size': args.batch_size,
            'shuffle': True,
            'num_workers': 4,
            'collate_fn': padded_collate,
            'pin_memory': True
        })

    valid_iterator_params = create_params(
        base='iterator_valid',
        param_dict={
            'num_workers': 4,
            'collate_fn': padded_collate,
            'pin_memory': True
        })

    module_params_ = {
        'n_embed': vocab_size,
        'd_out': num_labels,
        'embed_cls': embed_cls,
        
    }

    module_params_.update(vars(args))

    module_params = create_params(
        base='module',
        param_dict=module_params_)

    lr = optim_params.pop('lr')
    optim_params = create_params(
        base='optimizer',
        param_dict=optim_params)

    callbacks = [
        accuracy, f1_score, progress,
        early_stopping, scheduler
    ]

    net = net_cls(
        module=module_cls,
        optimizer=optim_cls,
        lr=lr,
        **module_params,
        **optim_params,
        criterion=SequenceNLLLoss,
        criterion__ignore_index=ignore_idx,
        dataset=EntityDataset,
        device=device,
        warm_start=warm_start,
        **train_iterator_params,
        **valid_iterator_params,
        callbacks=callbacks)

    print(vars(args))

    # creating model dir if it does not exist
    os.makedirs(model_dir, exist_ok=True)

    if args.grid_search:
        if args.param_grid is None:
            args.param_grid = join(
                abspath(dirname(__file__)),
                '..', 'configs',
                args.model + '-grid.json')

        print('Running grid search with params '
              'from {}'.format(args.param_grid))

        with open(args.param_grid, 'r') as fh:
            param_grid = json.load(fh)

        param_grid = parse_grid(param_grid)

        grid_search = GridSearchCV(
            estimator=net,
            param_grid=param_grid,
            cv=args.cross_folds,
            refit=False,
            scoring=accuracy_scoring)

        grid_search.fit(
            dataset, epochs=args.max_epochs)

        # saving best params and setting
        # training params with them
        param_path = join(model_dir, 'best_params.json')

        with open(param_path, 'w') as fh:
            json.dump({
                'score': grid_search.best_score_,
                'params': str(grid_search.best_params_)
            }, fh)

        pd.DataFrame(grid_search.cv_results_).to_csv(
            path_or_buf=join(model_dir, 'cv_result.csv'))

        print('Found best params: {}'.format(
            grid_search.best_params_))

        net.set_params(**grid_search.best_params_)

    elif warm_start:
        net.initialize()
        net.load_params(
            f_params=model_file,
            f_optimizer=optim_file,
            f_history=hist_file)

    # fitting either the best grid search
    # model, warm started model or a fresh model
    net.fit(dataset, epochs=args.max_epochs)

    net.save_params(
        f_params=model_file,
        f_optimizer=optim_file,
        f_history=hist_file)


if __name__ == '__main__':
    main()
