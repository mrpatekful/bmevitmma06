"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

# pylint: disable=not-callable
# pylint: disable=import-error
# pylint: disable=no-member
# pylint: disable=no-name-in-module

import argparse
import torch

from src.data import (
    setup_data_args,
    create_dataset,
    UNK)

from src.models import (
    setup_model_args,
    create_model_cls)

from os.path import join


def setup_eval_args():
    """
    Sets up the arguments for evaluation.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--cuda',
        type=bool,
        default=False,
        help='Device for evaluation.')

    setup_data_args(parser)
    setup_model_args(parser)

    return parser.parse_args()


def main():
    args = setup_eval_args()
    device = torch.device(
        'cuda' if args.cuda else 'cpu')

    model_dir = join(
        args.model_dir, args.data, args.model)

    state_dict = torch.load(
        join(model_dir, 'model.pt'),
        map_location=device)

    _, vocab = create_dataset(args)

    vocab_size = len(vocab['word_to_idx'])
    labels_size = len(vocab['tag_to_idx'])
        
    model_cls = create_model_cls(args)
    model = model_cls(vocab_size, labels_size)

    model.load_state_dict(state_dict)
    model.eval()

    w2i = vocab['word_to_idx']

    @torch.no_grad()
    def predict(text):
        """
        Translates the given text with beam search.
        """
        ids = [
            w2i.get(w.lower(), w2i[UNK]) for 
            w in text.split()
        ]
        
        inputs = torch.tensor([ids])
        outputs = model(inputs)

        return outputs

    print('Type a sentence to translate. ' + \
          'CTRL + C to escape.')
          
    while True:
        try:
            print()
            text = input()
            output = predict(text)
            print(output)
            print()

        except KeyboardInterrupt:
            break


if __name__ == '__main__':
    main()
