"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

# pylint: disable=not-callable
# pylint: disable=import-error
# pylint: disable=no-member
# pylint: disable=no-name-in-module

import argparse
import torch

from os.path import join

from torch.utils.data import DataLoader

from src.data import (
    setup_data_args,
    create_dataset,
    UNK)

from src.models import (
    setup_model_args,
    create_embed_cls,
    create_model_cls)

from src.metrics import (
    compute_conll_stats)

from src.collate import padded_collate


def setup_eval_args():
    """
    Sets up the arguments for evaluation.
    """
    parser = argparse.ArgumentParser()
    
    parser.add_argument(
        '--cuda',
        type=bool,
        default=False,
        help='Device for evaluation.')

    setup_data_args(parser)
    setup_model_args(parser)

    return parser.parse_args()


def main():
    args = setup_eval_args()
    device = torch.device(
        'cuda' if args.cuda else 'cpu')

    model_dir = join(
        args.model_dir, args.data, args.model, 
        args.name)

    state_dict = torch.load(
        join(model_dir, 'model.pt'),
        map_location=device)

    module_cls = create_model_cls(args)
    embed_cls = create_embed_cls(args)
    vocab_cls = embed_cls.get_vocab_cls()

    dataset, token_vocab, tag_vocab = \
        create_dataset(args, vocab_cls)

    vocab_size = len(token_vocab.token_to_idx)
    n_labels = len(tag_vocab.token_to_idx)
        
    model = module_cls(
        embed_cls=embed_cls, 
        n_embed=vocab_size,
        d_out=n_labels)

    model.load_state_dict(state_dict)
    model.eval()

    i2w = token_vocab.idx_to_token
    i2t = tag_vocab.idx_to_token

    print(compute_conll_stats(
        model=model,
        dataset=dataset,
        idx_to_word=i2w,
        idx_to_tag=i2t
    ))


if __name__ == '__main__':
    main()
