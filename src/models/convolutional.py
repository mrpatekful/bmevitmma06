"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

# pylint: disable=no-member

import torch

from torch.nn import ModuleList

from torch.nn.modules import (
    Embedding, Linear,
    Module, Dropout, Conv1d)

from torch.nn.functional import glu

from src.models.embedding import (
    SinusoidalEncoding)

from src.models.model import (
    EntityClassifier,
    create_crf_cls)


class CNN(EntityClassifier):
    """
    Convolutional entity recognizer.
    """

    name = 'cnn'

    @classmethod
    def setup_model_args(cls, parser):
        parser.add_argument(
            '--d_embed',
            type=int,
            default=128,
            help='Dimension of the embeddings.')
        parser.add_argument(
            '--d_model',
            type=int,
            default=256,
            help='Hidden dimension of the model.')
        parser.add_argument(
            '--n_layers',
            type=int,
            default=2,
            help='Number of layers in the model.')

    def __init__(
            self, d_out, d_embed=128, d_model=256,
            n_layers=2, **kwargs):
        super().__init__(d_embed=d_embed, **kwargs)

        self.pos = SinusoidalEncoding(d_embed)

        if d_embed != d_model:
            self.proj = Linear(d_embed, d_model)
        else:
            self.proj = Identity()

        self.layers = ModuleList([
            ConvBlock(
                d_input=d_model, d_out=d_model) 
            for _ in range(n_layers)
        ])

        self.linear = Linear(d_model, d_out)

    def forward(self, inputs):
        # mask is not needed only the token ids
        inputs = inputs[0]

        embedded = self.pos(self.embedding(inputs))
        hidden = self.proj(embedded)

        for layer in self.layers:
            hidden = layer(hidden)

        logits = self.linear(hidden)

        return logits


class ConvBlock(Module):
    """
    1D convolutional blocks with residual connection.
    """

    def __init__(self, d_input, d_out):
        super().__init__()

        self.convs = ModuleList([
            Conv1d(
                in_channels=d_input, 
                out_channels=d_out, 
                kernel_size=3,
                padding=1),
            Conv1d(
                in_channels=d_input, 
                out_channels=d_out, 
                kernel_size=5,
                padding=2)
        ])

        self.dropout = Dropout(p=0.1)

    def forward(self, inputs):
        # converting inputs to channel first
        # convolution input tensors
        inputs = inputs.permute(0, 2, 1)
        inputs = self.dropout(inputs)

        outputs = torch.cat([
            conv(inputs) for conv in self.convs
        ], dim=1)

        outputs = glu(outputs, dim=1)
        outputs = outputs.permute(0, 2, 1)

        return outputs


CNNCRF = create_crf_cls(CNN)
