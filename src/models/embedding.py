"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

# pylint: disable=no-member

import torch

from math import log

from torch.nn.modules import (
    Embedding, EmbeddingBag,
    Module, Dropout)

from src.data import (
    Vocab, CharVocab,
    HashVocab, MultiVocab,
    GloveVocab)

from src.utils import get_subclasses

from os.path import join


def create_embed_cls(args):
    """
    Creates the embedding cls.
    """
    return EMBEDDINGS[args.embed]


class SinusoidalEncoding(Module):
    """
    Applies positional encoding.
    """

    def __init__(self, d_embed, max_len=5000):
        super().__init__()

        self.dropout = Dropout(p=0.1)

        encoding = torch.zeros(max_len, d_embed)
        position = (
            torch.arange(0, max_len)
            .float()
            .unsqueeze(1))

        div_term = torch.exp(
            torch.arange(0, d_embed, 2).float() *
            -(log(10000.0) / d_embed))

        encoding[:, 0::2] = torch.sin(
            position * div_term)
        encoding[:, 1::2] = torch.cos(
            position * div_term)

        encoding = encoding.unsqueeze(0)
        encoding.requires_grad = False

        self.register_buffer('encoding', encoding)

    def forward(self, inputs):
        seq_len = inputs.size(1)

        outputs = inputs + self.encoding[:, :seq_len]
        outputs = self.dropout(outputs)

        return outputs


class EntityEmbedding(Module):
    """
    Base class for the embeddings of the models.
    """

    @classmethod
    def get_vocab_cls(cls):
        raise NotImplementedError('Abstract method.')

    @classmethod
    def setup_embed_args(cls, parser):
        pass


class WordEmbedding(EntityEmbedding):
    """
    Applies word embedding with possibly
    pretrained vectors.
    """

    @classmethod
    def get_vocab_cls(cls):
        return Vocab

    def __init__(
            self, n_embed, d_embed, 
            freeze_embed=False, weights=None, 
            **kwargs):
        super().__init__()

        if weights is not None:
            msg = 'Embeddings have {} dims but ' + \
                  '`--d_embed` is {}'
            assert d_embed == weights.size(1), \
                msg.format(weights.size(1), d_embed)

            self.embedding = Embedding.from_pretrained(
                weights, freeze=freeze_embed, 
                padding_idx=0)

        else:
            self.embedding = Embedding(
                n_embed, d_embed, padding_idx=0)

    def forward(self, inputs):
        return self.embedding(inputs)


class GloveEmbedding(WordEmbedding):
    """
    Pretrained word vectors with 
    Glove embeddings.
    """

    @classmethod
    def get_vocab_cls(cls):
        return GloveVocab

    @classmethod
    def setup_embed_args(cls, parser):
        super().setup_embed_args(parser)
        parser.add_argument(
            '--glove_file',
            type=str,
            default=GloveVocab.TYPES[0],
            choices=GloveVocab.TYPES,
            help='Name of the glove file to use.')
        parser.add_argument(
            '--freeze_embed',
            action='store_true',
            help='Freeze the weights of glove embedding.')

    def __init__(
            self, data, data_dir, glove_file, **kwargs):
        embed_path = join(
            data_dir, data, 
            glove_file + '.pt')

        weights = torch.load(embed_path)

        super().__init__(
            data=data, data_dir=data_dir, 
            weights=weights, **kwargs)

    
class CharEmbedding(EntityEmbedding):
    """
    Computes character level embeddings
    which are then merged into words.
    """

    @classmethod
    def get_vocab_cls(cls):
        return CharVocab

    def __init__(
            self, n_embed, d_embed, **kwargs):
        super().__init__()  

        self.embedding = EmbeddingBag(
            n_embed, d_embed, padding_idx=0)

    def forward(self, inputs, offsets):
        return self.embedding(inputs, offsets)


class HashEmbedding(EntityEmbedding):
    """
    Implements hash embedding from 
    https://arxiv.org/abs/1709.03933.
    """

    @classmethod
    def get_vocab_cls(cls):
        return HashVocab

    @classmethod
    def setup_embed_args(cls, parser):
        parser.add_argument(
            '--n_bucket',
            type=int,
            default=5000,
            help='Size the has table.')
        parser.add_argument(
            '--n_hash',
            type=int,
            default=3,
            help='Number of hashes.')
  
    def __init__(
            self, n_embed, d_embed, n_bucket, 
            n_hash, **kwargs):
        super().__init__()

        self.weight_emb = Embedding(
            d_embed, n_hash, padding_idx=0)

        self.bucket_emb = Embedding(
            n_bucket, d_embed, padding_idx=0)
      
    def forward(self, inputs):
        weight_ids = inputs[..., 0]
        bucket_ids = inputs[..., 1:]

        weights = self.weight_emb(weight_ids)
        outputs = self.bucket_emb(bucket_ids)

        weights = weights.unsqueeze(-1)
        weighted = (outputs * weights).sum(dim=-2)
        
        return weighted


class MultiEmbedding(EntityEmbedding):

    @classmethod
    def get_vocab_cls(cls):
        return MultiVocab

    def __init__(self, **kwargs):
        super().__init__()

        self.word_embed = WordEmbedding(**kwargs)
        self.char_embed = CharEmbedding(**kwargs)

    def forward(self, inputs):
        pass


EMBEDDINGS = {
    n[:-9].lower(): c for n, c in  
    get_subclasses(EntityEmbedding).items()
}
