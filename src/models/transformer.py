"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

# pylint: disable=no-member

import torch
import argparse

from math import log
from copy import deepcopy

from torch.nn.modules import Module

from torch.nn.modules import (
    Embedding, Linear, 
    Dropout, Identity,
    TransformerEncoder,
    TransformerEncoderLayer)

from src.models.model import (
    EntityClassifier,
    create_crf_cls)

from src.models.embedding import (
    SinusoidalEncoding)


class Transformer(EntityClassifier):
    """
    Self attentional entity recognizer.
    """

    name = 'trf'

    @classmethod
    def setup_model_args(cls, parser):
        parser.add_argument(
            '--d_embed',
            type=int,
            default=128,
            help='Dimension of the embeddings.')
        parser.add_argument(
            '--n_attn',
            type=int,
            default=4,
            help='Number of attention heads for the model.')
        parser.add_argument(
            '--d_model',
            type=int,
            default=128,
            help='Hidden dimension of the model.')
        parser.add_argument(
            '--d_ff',
            type=int,
            default=512,
            help='Dimension of the feed forward sub-model.')
        parser.add_argument(
            '--n_layers',
            type=int,
            default=4,
            help='Number of layers in the model.')

    def __init__(
            self, d_out, d_embed=128, n_attn=4, 
            d_model=128,  d_ff=512, n_layers=4, 
            **kwargs):
        super().__init__(d_embed=d_embed, **kwargs)

        self.pos = SinusoidalEncoding(d_embed)

        if d_embed != d_model:
            self.proj = Linear(d_embed, d_model)
        else:
            self.proj = Identity()

        self.transformer = TransformerEncoder(
            encoder_layer=TransformerEncoderLayer(
                d_model=d_model, 
                nhead=n_attn,
                dim_feedforward=d_ff),
            num_layers=n_layers)

        self.linear = Linear(d_model, d_out)

    def forward(self, inputs):
        inputs, mask = inputs[0], inputs[1]

        embedded = self.pos(self.embedding(inputs))
        hidden = self.proj(embedded)
        
        hidden = self.transformer(
            hidden.transpose(0, 1), 
            src_key_padding_mask=~mask.bool())

        hidden = hidden.transpose(0, 1)

        logits = self.linear(hidden)        

        return logits


TransformerCRF = create_crf_cls(Transformer)
