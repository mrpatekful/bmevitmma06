"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

from src.models.model import (
    setup_model_args,
    create_model_cls,
    CRFNeuralNet,
    SeqNeuralNet,
    score, multi_score,
    MODELS)

from src.models.embedding import (
    create_embed_cls)

from src.models.recurrent import *
from src.models.transformer import *
from src.models.convolutional import *


__all__ = [
    'setup_model_args',
    'create_model_cls',
]
