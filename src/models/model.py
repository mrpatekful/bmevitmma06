"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

# pylint: disable=no-member
# pylint: disable=no-name-in-module

import torch
import argparse

from skorch import NeuralNet
from skorch.setter import optimizer_setter
from skorch.utils import (
    TeeGenerator, to_tensor)

from torchcrf import CRF

from datetime import datetime
from statistics import mean, median
from torch.optim import SGD, Adam
from torch.utils.data import DataLoader

from torch.nn.functional import (
    log_softmax, softmax)

from torch.nn.modules import (
    Module, Embedding,
    Linear)

from os.path import (
    exists, join,
    dirname, abspath)

from src.collate import padded_collate
from src.models.embedding import EMBEDDINGS


def setup_model_args(parser):
    """
    Sets up the data arguments.
    """
    parser.add_argument(
        '--model',
        type=str,
        default='rnn_crf',
        choices=list(MODELS),
        help='Path of the data root directory.')
    parser.add_argument(
        '--name',
        type=str,
        default=datetime.today().strftime(
            '%y.%m.%d-%H:%M:%S'),
        help='Name of the resumed instance.')
    parser.add_argument(
        '--model_dir',
        type=str,
        default=join(abspath(
            dirname(__file__)), '..', '..', 'model'),
        help='Path of the data root directory.')
    parser.add_argument(
        '--embed',
        type=str,
        default='word',
        choices=list(EMBEDDINGS),
        help='Name of the embedding for training.')
    
    args, _ = parser.parse_known_args()
    MODELS[args.model].setup_model_args(parser)
    EMBEDDINGS[args.embed].setup_embed_args(parser)


MODELS = {}


def create_model_cls(args):
    """
    Returns a model class based on the `args.model`
    argument.
    """
    return MODELS[args.model]


class ModelMeta(type):
    """
    Meta class for registering models in
    `MODELS` dictionary.
    """

    def __new__(cls, name, bases, namespace):
        cls = super().__new__(
            cls, name, bases, namespace)

        name = cls.__name__.lower() if \
            cls.name is None else cls.name

        if cls.name is not None:
            MODELS[name] = cls

        return cls


class EntityClassifier(Module, metaclass=ModelMeta):
    """
    Base class for entity classifier models.
    """

    name = None

    @classmethod
    def get_net_cls(cls):
        """
        Returns either `SeqNeuralNet` or
        `CRFNeuralNet`.
        """
        return SeqNeuralNet

    @classmethod
    def get_data_cls(cls):
        """
        Returns the data class for the module.
        """
        raise NotImplementedError('Abstract method.')

    @classmethod
    def setup_model_args(cls, parser):
        """
        Sets up the arguments for the model.
        """
        raise NotImplementedError('Abstract method.')

    def __init__(self, embed_cls, **kwargs):
        super().__init__()
        
        self.embedding = embed_cls(**kwargs)

    def decode(self, inputs):
        mask = inputs[1]
        batch_size, seq_len = mask.size()

        logits = self(inputs)

        probs = softmax(logits, dim=-1)
        _, preds = probs.max(dim=-1)
        
        return [
            preds[idx, :seq_len - ~mask[idx].sum()].tolist()
            for idx in range(batch_size)
        ]


def create_crf_cls(super_class):
    """
    Creates a subclass with crf classifier head.
    """
    class crf_(super_class):
        """
        Base class for the CRF models.
        """

        name = super_class.name + '_crf'

        @classmethod
        def get_net_cls(cls):
            return CRFNeuralNet

        def __init__(self, *args, d_out, **kwargs):
            super().__init__(
                *args, d_out=d_out, **kwargs)

            self.crf = CRF(
                num_tags=d_out,
                batch_first=True)

        def forward(self, inputs, targets):
            mask = inputs[1]

            logits = super().forward(inputs)

            loss = self.crf(
                emissions=logits, 
                tags=targets,
                mask=mask.bool())

            loss /= mask.long().sum()

            return -loss

        def decode(self, inputs):
            mask = inputs[1]

            logits = super().forward(inputs)

            preds = self.crf.decode(
                emissions=logits,
                mask=mask.bool())

            return preds

    return crf_


class EntityClassifierNeuralNet(NeuralNet):
    """
    Base class for entity classifier neural nets.
    """

    def initialize_optimizer(
            self, triggered_directly=True):
        args, kwargs = self._get_params_for_optimizer(
            'optimizer', 
            self.module_.named_parameters())

        if self.initialized_ and self.verbose:
            msg = self._format_reinit_msg(
                "optimizer", kwargs, 
                triggered_directly=triggered_directly)
            print(msg)

        # lr is included in optimizer params and is
        # not added sepearately

        self.optimizer_ = self.optimizer(
            *args, **kwargs)

        self._register_virtual_param(
            ['optimizer__param_groups__*__*', 
             'optimizer__*'],
            optimizer_setter)


class SeqNeuralNet(EntityClassifierNeuralNet):
    """
    Special version of `NeuralNet` that views
    the output steps as independent actions.
    """

    def compute_preds(self, inputs):
        """
        Computes the preds for the score fn.
        """
        outputs = self.infer(inputs)
        outputs = to_tensor(
            outputs, device=self.device)

        # the output of the module
        # is the log_softmax
        outputs = outputs.reshape(
            -1, outputs.size(-1))

        probs = softmax(outputs, dim=-1)
        _, preds = probs.max(dim=-1)

        return preds


class CRFNeuralNet(EntityClassifierNeuralNet):
    """
    Special version of `NeuralNet` that uses CRF
    as the final module.
    """

    def compute_preds(self, inputs):
        """
        Computes the preds for the score fn.
        """
        inputs = to_tensor(
            inputs, device=self.device)

        outputs = self.module_.decode(inputs)
        # padding back the values so metric_fn
        # can be implemented consistently
        outputs = [
            preds + [-1] * (
                inputs.size(-1) - len(preds)) 
            for preds in outputs
        ]

        preds = torch.as_tensor(outputs)
        preds = preds.view(-1)

        return preds

    def infer(self, x, **fit_params):
        # this method is not used instead the
        # `Module` must have a decode fn that
        # produces tag results without grads
        raise NotImplementedError(
            'This method shouldn\'t be used.')

    def train_step_single(self, Xi, yi, **fit_params):
        y_true = to_tensor(yi, device=self.device)
        x = to_tensor(Xi, device=self.device)

        # sampling the current model for the predictions
        # this is done in eval mode
        self.module_.eval()
        with torch.no_grad():
            y_pred = self.module_.decode(x)

        # switching to train mode for the optimization
        self.module_.train()

        # computing loss from CRF so the probability
        # of generating this sequence is increased
        loss = self.module_(x, y_true)
        loss.backward()

        self.notify(
            'on_grad_computed',
            named_parameters=TeeGenerator(
                self.module_.named_parameters()),
            X=Xi,
            y=yi
        )

        return {'loss': loss, 'y_pred': y_pred}

    def validation_step(self, Xi, yi, **fit_params):
        self.module_.eval()

        x = to_tensor(Xi, device=self.device)
        y_true = to_tensor(yi, device=self.device)

        with torch.no_grad():
            y_pred = self.module_.decode(x)
            loss = self.module_(x, y_true)   

        return {'loss': loss, 'y_pred': y_pred}


def score(net, X_test, y_test, metric_fn):
    """
    Computes the result of the model on a
    specific metric.
    """
    dataset = X_test

    loader = DataLoader(
        dataset,
        batch_size=128,
        pin_memory=True,
        collate_fn=padded_collate)

    def evaluate():
        """
        Yields evaluation results.
        """
        net.module_.eval()
        
        for inputs, targets in loader:
            # `CRFNeuralNet` instance is used with
            # modules that have a CRF output
            preds = net.compute_preds(inputs)
            targets = targets.to(preds.device)

            results = metric_fn(
                preds=preds, 
                targets=targets)

            # filtering nan values
            not_nan = results[results == results]

            yield not_nan.mean().item()

    return mean(evaluate())


def multi_score(
        net, X_test, y_test, metric_fn):
    """
    Computes the result of the model on a
    specific metric with multiple outputs.
    """
    dataset = X_test

    loader = DataLoader(
        dataset,
        batch_size=128,
        pin_memory=True,
        collate_fn=padded_collate)

    def evaluate():
        """
        Yields evaluation results.
        """
        net.module_.eval()
        
        for inputs, targets in loader:
            preds = net.compute_preds(inputs)
            targets = targets.to(preds.device)

            results = metric_fn(
                preds=preds, 
                targets=targets)

            yield results.tolist()

    results = []
    for output in zip(*evaluate()):
        values = [v for v in output if v == v]
        result = mean(values) if len(values) > 0 \
            else 0

        results.append(result)

    return {
        'min': min(results), 
        'max': max(results),
        'mean': mean(results), 
        'median': median(results)
    }
