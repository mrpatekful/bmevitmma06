"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

from copy import deepcopy

from torch.nn.modules import (
    Embedding, Linear,
    LSTM, GRU)

from src.models.model import (
    EntityClassifier,
    create_crf_cls)


RECURRENT = {'lstm': LSTM, 'gru': GRU}


class BiRNN(EntityClassifier):
    """
    Bidirectional recurrent neural network for
    entity recognition.
    """

    name = 'rnn'

    @classmethod
    def setup_model_args(cls, parser):
        parser.add_argument(
            '--d_embed',
            type=int,
            default=128,
            help='Dimension of the embeddings.')
        parser.add_argument(
            '--d_model',
            type=int,
            default=256,
            help='Hidden dimension of the model.')
        parser.add_argument(
            '--n_layers',
            type=int,
            default=2,
            help='Number of layers in the model.')
        parser.add_argument(
            '--recurrent',
            type=str,
            choices=list(RECURRENT),
            default='lstm',
            help='Type of the recurrent unit.')

    def __init__(self, d_out, d_embed=128, d_model=256,
                 n_layers=2, recurrent='lstm',
                 **kwargs):
        super().__init__(d_embed=d_embed, **kwargs)

        recurrent = RECURRENT[recurrent]

        self.recurrent = recurrent(
            input_size=d_embed,
            hidden_size=d_model,
            num_layers=n_layers,
            dropout=0.1,
            batch_first=True,
            bidirectional=True)

        self.linear = Linear(d_model * 2, d_out)

    def forward(self, inputs):
        # mask is not needed only the token ids
        inputs = inputs[0]

        embedded = self.embedding(inputs)
        hidden, _ = self.recurrent(embedded)
        logits = self.linear(hidden)

        return logits


BiRNNCRF = create_crf_cls(BiRNN)
