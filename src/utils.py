"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

import os
import re
import torch
import random
import urllib

import numpy as np

from itertools import product, chain
from tqdm import tqdm


def set_random_seed(args):
    """
    Sets the random seed for training.
    """
    torch.manual_seed(args.seed)
    random.seed(args.seed)
    np.random.seed(args.seed)

    if args.cuda:
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False


def parse_grid(param_grid, referables):
    """
    Replaces the alias$<module>$ references
    with their corresponding python module.
    """
    def replace(elem):
        """
        Replaces the references in the leaf
        values with their referenced type.
        """
        match = re.search(
            r'(alias\$)(.*)(\$)', str(elem))
        # NOTE currently only optimizer or models
        # can be referenced

        if match is not None:
            result = match.group(2)
            if result in referables:
                return referables[result]

            else:
                raise RuntimeError(
                    '{} is not in {}'.format(
                        result, ','.join(referables)))
        else:
            return elem

    def is_value(elem):
        """
        Returns true if the value is non-iterable.
        """
        return type(elem) in {str, int, float, bool}

    def walk(collection, iterator):
        """
        Iterates through the provided param grid
        recursively.
        """
        for key, elem in iterator:
            if is_value(elem):
                collection[key] = replace(elem)
            else:
                if isinstance(elem, list):
                    walk(elem, enumerate(elem))
                else:
                    walk(elem, elem.items())

        return collection

    def maybe_wrap(value):
        """
        Wraps a value in a list if it not a list.
        """
        return value if isinstance(value, list) \
            else [value]

    def create_dict(dict_tuple):
        """
        Creates a single from a tuple of dicts.
        """
        result_dict = {}
        for d in dict_tuple:
            result_dict.update(d)

        return result_dict

    param_grid = walk(
        param_grid, enumerate(param_grid)) \
        if isinstance(param_grid, list) else \
        walk(param_grid, param_grid.items())

    param_grid = list(map(maybe_wrap, param_grid))
    param_grid_ = param_grid[0]

    for group in param_grid[1:]:
        param_grid_ = [
            create_dict(sub_group)
            for sub_group in
            product(group, param_grid_)
        ]

    return param_grid_


def get_subclasses(cls):
    """
    Returns a dict with the names of subclasses
    of a class.
    """
    def generate_subclasses(c):
        for s in c.__subclasses__():
            # recursively runs through the
            # subclasses of dialog datasets
            yield from generate_subclasses(s)
            yield s

    # filtering out possible duplicates
    subclasses = set(generate_subclasses(cls))

    return {s.__name__: s for s in subclasses}


def create_params(base, param_dict):
    """
    Converts param dicts to sklearn
    param format.
    """
    return {
        base + '__' + key: param_dict[key]
        for key in param_dict
    }


class reporthook(tqdm):
    """
    Simple reporthook implementation from
    https://pypi.org/project/tqdm/.
    """

    def update_to(self, b=1, bsize=1, tsize=None):
        if tsize is not None:
            self.total = tsize

        self.update(b * bsize - self.n)


def download(url, filename):
    """
    Downloads an url to the filename.
    """
    with reporthook(
            unit='B', unit_scale=True, 
            miniters=1, desc=url.split('/')[-1]) as pb:
        urllib.request.urlretrieve(
            url, filename=filename, 
            reporthook=pb.update_to, data=None)
