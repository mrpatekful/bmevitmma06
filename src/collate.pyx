"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entitiy-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.06.25.
"""

# distutils: language=c++

cimport numpy as np
import numpy as np

import cython
import torch

from libcpp.vector cimport vector
from libcpp cimport bool

from libc.stdint cimport int32_t


def padded_collate(vector[vector[vector[int]]] examples):
    """
    Collate function for merging a list of examples into
    a batch tensor.
    """
    cdef vector[vector[int]] words, labels, sample
    cdef Py_ssize_t btc_idx, btc_size = examples.size()

    cdef int seq_len, max_len = 0

    for btc_idx in range(btc_size):
        # source size length
        sample = examples[btc_idx]

        seq_len = sample[0].size()

        if seq_len > max_len:
            max_len = seq_len

        words.push_back(sample[0])
        labels.push_back(sample[1])

    cdef np.ndarray[np.int32_t, ndim=3] word_tensor = \
        batchify(words, max_len, 0)

    cdef np.ndarray[np.int32_t, ndim=2] label_tensor = \
        batchify(labels, max_len, -1)[0]

    return torch.as_tensor(word_tensor).long(), \
        torch.as_tensor(label_tensor).long()


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
cdef batchify(vector[vector[int]] ids, 
              const int max_len, 
              const int pad_val):
    """
    Creates a batch from a list of utterances.
    """
    cdef Py_ssize_t btc_size = ids.size()
    cdef Py_ssize_t btc_idx

    cdef np.ndarray[np.int32_t, ndim=3] tensor = \
        np.ones([2, btc_size, max_len], dtype=np.int32)

    cdef Py_ssize_t utr_size, idx, diff_size, \
        diff_idx, pad_idx

    for btc_idx in range(btc_size):
        utr_size = ids[btc_idx].size()
        diff_size = max_len - utr_size

        for idx in range(utr_size):
            tensor[0, btc_idx, idx] = \
                ids[btc_idx][idx]

        for diff_idx in range(diff_size):
            pad_idx = utr_size + diff_idx 
            tensor[0, btc_idx, pad_idx] = pad_val
            tensor[1, btc_idx, pad_idx] = 0

    return tensor
