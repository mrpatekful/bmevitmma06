"""
@author:    Patrik Purgai
@copyright: Copyright 2019, entity-recognition
@license:   MIT
@email:     purgai.patrik@gmail.com
@date:      2019.09.11.
"""

# pylint: disable=no-name-in-module
# pylint: disable=import-error

import csv
import json
import torch
import hashlib
import shutil
import zipfile
import os
import random

from tqdm import tqdm
from collections import Counter
from itertools import chain
from math import sqrt
from random import randint
from functools import partial

from os.path import (
    exists, join,
    dirname, abspath)

from torch.utils.data import (
    Dataset, DataLoader,
    Sampler)

from src.collate import padded_collate
from src.utils import download

from torch.utils.data import Dataset


PAD = '<pad>'
UNK = '<unk>'


def setup_data_args(parser):
    """
    Sets up the data arguments.
    """
    parser.add_argument(
        '--data',
        type=str,
        default='course',
        choices=list(os.listdir(
            join(dirname(__file__), '..', 'data'))),
        help='Name of the data')
    parser.add_argument(
        '--data_dir',
        type=str,
        default=join(abspath(
            dirname(__file__)), '..', 'data'),
        help='Path of the data root directory.')
    parser.add_argument(
        '--download_dir',
        type=str,
        default=join(abspath(
            dirname(__file__)), '..', 'data'),
        help='Path of the download directory.')
    parser.add_argument(
        '--vocab_size',
        type=int,
        default=40000,
        help='Maximum size of the vocabulary.')
    parser.add_argument(
        '--min_freq',
        type=int,
        default=0,
        help='Minimum frequency of a word to be in vocab.')
    parser.add_argument(
        '--max_len',
        type=int,
        default=500,
        help='Maximum size of a sequence. Truncated if longer.')
    parser.add_argument(
        '--lower',
        action='store_true',
        help='Lower the words in the data.')


def transform(
        args, data_path, read_fn, token_vocab, 
        tag_vocab):
    """
    Transforms the dataset to numericalized format.
    """
    lines = read_fn(data_path, args.lower)
    for words, tags in lines:
        word_ids = token_vocab.encode(words)
        tag_ids = tag_vocab.encode(tags)

        yield word_ids, tag_ids


def hash_word(word):
    """
    Converts a word to hashed representation.
    """
    encoded = word.encode('utf-8')
    num = int(hashlib.sha1(encoded).hexdigest(), 16)

    return num


def apply_hash(
        value, bucket_size, a=1, b=0,
        moduler=None, offset=1):
    """
    Applies hash to a given value.
    """
    value = a * value + b
    if moduler is not None:
        value = value % moduler

    return value % (bucket_size - 1) + 1


def compute_next_prime(value):
    """
    Naively gets the next prime larger than `value`.
    """
    def is_prime(x):
        """Naive is prime test."""
        for i in range(2, int(sqrt(x))):
            if x % i == 0:
                return False
        return True

    while not is_prime(value):
        value += 1

    return value


def generate_ngrams(text, n=3):
    """
    Creates ngrams from a given text.
    """
    for idx in range(max(len(text) - n + 1, 1)):
        yield text[idx:idx + n]


def draw_hashes(moduler, num_hashes):
    """
    Computes `num_hashes` number of prime addends 
    and factors.
    """
    addends = set()
    factors = set()

    def get_next(values):
        value = randint(1, moduler - 1)
        while value in values:
            value = randint(1, moduler - 1)
        values.add(value)

        return value

    hashes = [
        (get_next(factors), get_next(addends))
        for _ in range(num_hashes)
    ]

    return hashes


class Vocab:
    """
    Base class for the different vocabularies.
    """

    @classmethod
    def get_vocab_path(cls, save_dir):
        return join(
            save_dir, 
            cls.__name__.lower() + '.json')

    @classmethod
    def load(cls, args, save_dir=None, save_path=None):
        assert (save_dir is None) ^ (save_path is None), \
            'Exactly one of `save_dir` or `save_path`' + \
            ' must be given.'

        if save_path is None:
            save_path = cls.get_vocab_path(save_dir)
        
        with open(save_path, 'r') as fh:
            return cls(args, **json.load(fh))

    def save(self, save_dir=None, save_path=None):
        assert (save_dir is None) ^ (save_path is None), \
            'Exactly one of `save_dir` or `save_path`' + \
            ' must be given.'

        if save_path is None:
            save_path = self.get_vocab_path(save_dir)
        
        with open(save_path, 'w') as fh:
            json.dump({
                'token_freqs': self.token_freqs,
                'token_to_idx': self.token_to_idx
            }, fh)

    @classmethod
    def build(cls, args, reader_fn, **kwargs):
        """
        Builds the vocab over the files in `data_path`.
        """
        data_dir = join(args.data_dir, args.data)
        data_path = join(data_dir, 'data.txt')

        vocab = cls(args, **kwargs)

        for example in reader_fn(data_path):
            vocab.update(example)

        return vocab

    def __init__(
            self, args, token_freqs=None, 
            token_to_idx=None):
        self.args = args

        if token_freqs is None:
            token_freqs = Counter()

        if token_to_idx is None:
            token_to_idx = {PAD: 0, UNK: 1}

        self.token_freqs = token_freqs
        self.token_to_idx = token_to_idx
        self.idx_to_token = {
            str(i):t for t, i 
            in token_to_idx.items()
        }

    def encode(self, text):
        """
        Converts the text to id(s).
        """
        result = []
        for word in text:
            result.extend([
                self.token_to_idx.get(
                    token, self.token_to_idx.get(UNK))
                for token in self._process(word)
            ])

        assert None not in result, \
            'Unknown word in text without having an ' + \
            '<unk> token included in the vocabulary.'

        return result

    def _process(self, text):
        """
        Converts a text to vocab format.
        """
        return [text]

    def update(self, texts):
        """
        Updates the vocab with a list of texts.
        """
        for text in texts:
            for token in self._process(text):
                if token not in self.token_freqs:
                    self.token_to_idx[token] = len(
                        self.token_to_idx)

                self.token_freqs[token] += 1

    def finalize(self, args):
        """
        Truncates the vocab and takes the
        least frequent elements.
        """
        most_common = self.token_freqs.most_common(
            self.args.vocab_size)

        pad_id = self.token_to_idx.pop(PAD, None)
        unk_id = self.token_to_idx.pop(UNK, None)

        token_to_idx = {}

        if pad_id is not None:
            token_to_idx[PAD] = 0

        if unk_id is not None:
            token_to_idx[UNK] = len(token_to_idx)

        for token, freq in most_common:
            if freq >= self.args.min_freq:
                token_to_idx[token] = len(token_to_idx)

        self.token_to_idx = token_to_idx
        self.idx_to_token = {
            str(i):t for t, i 
            in token_to_idx.items()
        }

            
class CharVocab(Vocab):
    
    def _process(self, text):
        """
        Converts the text to chars.
        """
        return [c for c in text]


class HashVocab(Vocab):

    def __init__(self, ):
        pass

    def encode(self):
        pass


class MultiVocab(Vocab):

    def __init__(self, ):
        pass

    def encode(self):
        pass


class GloveVocab(Vocab):

    URL = 'http://nlp.stanford.edu/data/{}'

    TYPES = (
        'glove.840B.300d',
        'glove.42B.300d',
        'glove.6B',
        'glove.twitter.27B'
    )

    def __init__(self, args, **kwargs):
        super().__init__(args, **kwargs)
        
        download_path = join(
            args.data_dir, args.glove_file + '.zip')

        extract_path = join(
            args.data_dir, args.glove_file + '.txt')
        
        if not exists(download_path) and \
                not exists(extract_path):
            download(
                self.URL.format(args.glove_file) + '.zip',
                download_path)

        if not exists(extract_path):
            with zipfile.ZipFile(download_path) as zf:
                zf.extractall(args.data_dir)

        self.vectors_path = extract_path

    def finalize(self, args):
        vectors = {}
        d_embed = set()

        with open(self.vectors_path, 'r') as fh:
            for line in tqdm(
                    fh, desc='syncing vocab', leave=False):
                split = line.split(' ')
                token, vector = split[0], split[1:]

                if token in self.token_to_idx:
                    vectors[token] = [
                        float(n) for n in vector]
                    d_embed.add(len(vectors[token]))

        msg = 'Multiple dimensions ({}) for embedding.'
        assert len(d_embed) == 1, msg.format(d_embed)
            
        d_embed = list(d_embed)[0]
            
        token_freqs = Counter()

        for token in self.token_freqs:
            if token in vectors:
                token_freqs[token] = \
                    self.token_freqs[token]

        msg = 'Vector found for {} tokens out of {}'
        print(msg.format(
            len(token_freqs), len(self.token_freqs)))

        self.token_freqs = token_freqs

        super().finalize(args)

        embeds = torch.empty((
            len(self.token_to_idx), d_embed))

        pad_id = self.token_to_idx.pop(PAD, None)
        unk_id = self.token_to_idx.pop(UNK, None)

        for token, idx in self.token_to_idx.items():
            embeds[idx] = torch.tensor(vectors[token])

        if pad_id is not None:
            embeds[pad_id] = torch.zeros((d_embed, ))
            self.token_to_idx[PAD] = pad_id

        if unk_id is not None:
            embeds[unk_id] = torch.ones((d_embed, ))
            self.token_to_idx[UNK] = unk_id

        embed_path = join(
            args.data_dir, args.data, 
            args.glove_file + '.pt')

        torch.save(embeds, embed_path)


def read_conll(data_path, lower=True):
    """
    Reads the contents of a conll format file.
    """
    words, tags = [], []

    with open(data_path, 'r') as fh:
        for line in fh:
            if line.strip() == '':
                yield words, tags

                words, tags = [], []

            else:
                word, tag, *_ = line.split()

                words.append(
                    word.lower() if 
                    lower else word)

                tags.append(
                    tag.lower() if 
                    lower else tag)


def read_csv(data_path, lower=True):
    """
    Reads the given .csv file and returns it as 
    a list of lists.
    """
    words, tags = [], []

    with open(data_path, 'r', encoding='latin-1') as fh:
        csv_reader = csv.reader(fh, delimiter=';')

        for row in csv_reader:
            if row[0].strip() != '':
                if len(words) > 0:
                    yield words, tags

                words, tags = [], []

            else:
                word, tag = row[1], row[3]

                words.append(
                    word.lower() if 
                    lower else word)

                tags.append(
                    tag.lower() if 
                    lower else tag)


class EntityDataset(Dataset):

    def __init__(self, examples, y=None, **kwargs):
        self.examples = examples

    def __getitem__(self, idx):
        return self.examples[idx]

    def __len__(self):
        return len(self.examples)


def create_dataset(args, vocab_cls):
    """
    Converts the dataset into numericalized format
    and returns iterators over the train and
    test splits.
    """
    data_dir = join(args.data_dir, args.data)

    data_path = join(data_dir, 'dataset.pt')
    vocab_path = vocab_cls.get_vocab_path(data_dir)
    tags_path = join(data_dir, 'tags.json')

    if not exists(vocab_path):
        msg = ' ** No token vocab file found. ' + \
            'Creating and saving new vocab ' + \
            'to {} **'
        
        reader_fn = partial(
            read_file_fns[args.data],
            lower=args.lower)

        token_vocab = vocab_cls.build(
            args, 
            lambda p: (w for w, _  in reader_fn(p)))
        token_vocab.finalize(args)

        token_vocab.save(save_path=vocab_path)

    else:
        msg = '** Token vocab file found. Loading ' + \
              'it from {} **'

        token_vocab = vocab_cls.load(
            args, save_path=vocab_path)
        
    print(msg.format(vocab_path))

    if not exists(tags_path):
        msg = '** No tag vocab file found. ' + \
            'Creating and saving new vocab ' + \
            'to {} **'
        
        reader_fn = partial(
            read_file_fns[args.data],
            lower=args.lower)

        tag_vocab = Vocab.build(
            args, 
            lambda p: (t for _, t  in reader_fn(p)), 
            token_to_idx={})

        tag_vocab.save(save_path=tags_path)

    else:
        msg = '** Tag vocab file found. Loading ' + \
              'it from {} **'
            
        tag_vocab = Vocab.load(
            args, save_path=tags_path)
        
    print(msg.format(tags_path))

    if not exists(data_path):
        # if dataset does not exist then create it
        # downloading and tokenizing the raw files
        raw_path = join(
            args.data_dir, args.data, 'data.txt')

        read_fn = read_file_fns[args.data]

        dataset = list(transform(
            args=args, 
            token_vocab=token_vocab,
            tag_vocab=tag_vocab,
            data_path=raw_path, 
            read_fn=read_fn))

        torch.save(dataset, data_path)

    else:
        dataset = torch.load(data_path)

    return dataset, token_vocab, tag_vocab


read_file_fns = {
    'conll': read_conll,
    'course': read_csv
}
